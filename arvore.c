#include "stdio.h"
#include "stdlib.h"
#include "arvore.h"

#define MAX 5
struct nodo * insere_nodo(struct nodo * raiz, int valor){
	
	struct nodo *temp;
	
	if(raiz == NULL){
		temp = malloc(sizeof(struct nodo));
		temp->valor = valor;
		temp->esq = NULL;
		temp->dir = NULL;
		return temp;
	}
	
	else
	{
		if(raiz->valor > valor){
			raiz->esq = insere_nodo(raiz->esq, valor);
			return raiz;	
		}
		else
		{
			if(raiz->valor < valor){
				raiz->dir = insere_nodo(raiz->dir, valor);
				return raiz;	
			}
			
				
		}
		return raiz;
	}
	
		return raiz;		
}

struct nodo * inicializa_arvore(int entradas, int * valores){
	
	struct nodo *raiz = NULL;
	
	int i = 0;
	if(entradas <= 0 || valores == NULL)
		return NULL;

	else{
		raiz =insere_nodo(raiz, valores[i]);
		if(entradas == 1){
			
			//printf("%d ", raiz->valor);	
			return raiz;
		}	
		else{
		for (i = 1; i < entradas ; i++)
			{	
							
				raiz =insere_nodo(raiz, valores[i]);	
					
			}
			
				
		}
	}
	
	return raiz;
	
}

struct nodo *encontra_minimo(struct nodo *raiz){
		while (raiz->esq != NULL)
		{
			raiz = raiz->esq;
		}
	return raiz;	
}

struct nodo * remove_nodo(struct nodo * raiz, int valor){

	struct nodo *temp;


	if(raiz == NULL)
		return raiz;

	else{

		if(raiz->valor > valor)
			raiz->esq = remove_nodo(raiz->esq, valor);

		else{

			if(raiz->valor < valor)
				raiz->dir = remove_nodo(raiz->dir, valor);

			else{
				if (raiz->esq != NULL && raiz->dir != NULL)
				{
					temp = encontra_minimo(raiz->dir);	
									
					raiz->valor = temp->valor;					
					raiz->dir = remove_nodo(raiz->dir, raiz->valor);								
				}	
				
					else
					{
						if(raiz->esq == NULL) 		
							temp = raiz->dir;
						
						else
							temp = raiz->esq;
							
							
						free(raiz);	
						return temp;																
					}		
							
				}
		
			}
	}
	return raiz;
}

struct nodo * busca(struct nodo * raiz, int valor){
	
	if(raiz == NULL){
		return NULL;
	}
	
	else
	{
		if(raiz->valor == valor)
			return raiz;
			
		else if(raiz->valor < valor)
			return busca(raiz->esq, valor);
			
		else
			return busca(raiz->dir, valor);
				
	}

}

int altura(struct nodo * raiz){
	int direita = 0;
	int esquerda = 0;


	if(raiz == NULL)
		return 0;

	else{
		esquerda = altura(raiz->esq);
		direita = altura (raiz->dir);
	}

	if(direita > esquerda)
		return direita+1;
	else if(direita < esquerda)
		return esquerda+1;
		
	else
		return direita+1;
		

}
int k = 0;

int infix(struct nodo * raiz, int * resultado){
	
	int j;
				
	if(raiz == NULL){
		return k;
		
	}
	else{
			
			k = infix(raiz->esq, resultado);		
			resultado[k] = raiz->valor;	
			k++;						
			k=infix(raiz->dir, resultado);
				
		}
		j = k;
		k = 0;
		return j;	
}


int prefix(struct nodo * raiz, int * resultado){
	
	int j;
	if(raiz == NULL)
		return k;

	else{
			resultado[k] = raiz->valor;	
			k++;
			k = prefix(raiz->esq, resultado);											
			k=prefix(raiz->dir, resultado);
		}
	j = k;
	k = 0;
	return j;	

}

int postfix(struct nodo * raiz, int * resultado){
	
	//int j = 0;
	
	if(raiz==NULL)
		return k;

	else{
			
			k = postfix(raiz->esq, resultado);
			
			k=postfix(raiz->dir, resultado);			
			resultado[k] = raiz->valor;
			k++;
			//printf("%d\n", resultado[k]);
										
	}
	
	int l = k;

	k = 0;
	return l;	
}

int balanceada(struct nodo * raiz){


		int alturadir ;
		int alturaesq ;
		int retorno = 0;	

		alturadir = altura(raiz->dir);

		alturaesq = altura(raiz->esq);

		if(alturadir > alturaesq){

			retorno = alturadir - alturaesq;
			return retorno+1;	

		}

		else{
			if(alturaesq > alturadir){
					retorno = alturaesq - alturadir;
					return retorno+1;	
			}	

		}		
		return retorno+1;	
}

void imprime(int * valores, int tamanho){
	
	int i;
	int contador = 0;

	for (i = 0; i < tamanho; ++i)
	{
		printf("%d ", valores[i]);
		if(contador == 10){
			printf("\n");
			contador = 0;
		}
		contador++;
	}
}

int numero_elementos(struct nodo * raiz){

		int retorno = 0;
		

		if(raiz == NULL)
			return 0;

		else{
					
				retorno = numero_elementos(raiz->esq) + numero_elementos(raiz->dir) + 1;

		}		

			return retorno;

}

int abrangencia(struct nodo * raiz, int * resultado){

	
	
	struct nodo* vetor[40];
	int qtd = 0, i=0, j;
	
	struct nodo* aux;
	
	
	j = numero_elementos(raiz);
	//printf("%d ELEMENTOS", j);
	
	
	if(raiz == NULL)
		return 0;
		
	else
	{
		
		vetor[0] = raiz;
		qtd++;
		
	
		while (qtd < j )
		{
			aux = vetor[i];	
			i++;
			
			
			if(aux->esq != NULL){
				vetor[qtd] = aux->esq;
				qtd++;
				
			}	
			if(aux->dir != NULL){
				vetor[qtd] = aux->dir;
				qtd++;
					
			}	
		}
		
		for (i = 0; i < j; i++)
		{
			resultado[i] = vetor[i]->valor;
		}
		
		
	}
	
	
	
	return i;
	
}



void remove_todos(struct nodo * raiz){

	if(raiz == NULL)
		return;
		//nao faz nada

	else{
		remove_todos(raiz->esq);
		remove_todos(raiz->dir);
		free(raiz);
	}

}
